package stepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class TorkUSA {
	   WebDriver driver;
	   
	   
	 	 @Given("^user logs into the application$")
	 	 public void user_logs_into_the_application(){
	 	 System.setProperty("webdriver.chrome.driver","/Users/naveenkhunteta/Downloads/chromedriver");
	 	 driver = new ChromeDriver();
	 	 driver.get("https://www.freecrm.com/index.html");
	 	 }
	 	
	 	
	 	 @When("^title of login page is TorkUSA$")
	 	 public void title_of_login_page_is_TorkUSA(){
	 	 String title = driver.getTitle();
	 	 System.out.println(title);
	 	 Assert.assertEquals("#1 Free CRM for Any Business: Online Customer Relationship Software", title);
	 	 }
}
