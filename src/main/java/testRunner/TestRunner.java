package testRunner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(
		features = "", //the path of the feature files
		glue={"stepDefinitions"}, //the path of the step definition files
		tags={"@345673"},
		plugin = {"pretty" ,"html:Folder_Name" ,
				"json:Folder_Name/cucumber.json" ,
		"junit:Folder_Name/cucumber.xml"},
		monochrome = true, //display the console output in a proper readable format
		strict = true, //it will check if any step is not defined in step definition file
		dryRun = false //to check the mapping is proper between feature file and step def file	
		)

public class TestRunner {

}

